Había una vez un niño llamado Daniel, que era muy fanático de los videojuegos.

Cuando salió de la escuela, corrió a la tienda donde podía jugar, pero cuando llegó, solo había dos máquinas disponibles y una de ellas tenía un cartel de "fuera de servicio".

Corrió hacia el que estaba operativo, pero un niño le ganó la carrera, y Daniel, en lugar de irse a casa, comenzó a buscar una máquina de realidad virtual rota.

No sabía a qué jugaba, pero lo siguiente que vio fue un rayo de luz azul y en unos segundos estaba en un lugar totalmente diferente.

Todo a su alrededor era de colores brillantes y comenzaron a aparecer figuras gráficas que formaban torres y caminos. Además, justo en frente de Daniel, apareció un gran pasillo que le recordó a las carreteras.

Mientras caminaba por ese pasillo vio una galleta flotando y tuvo la intuición de que debía tomarla. Lo agarró y se lo comió.

Al hacerlo, escuchó un sonido: "clin". De repente, comenzó a ver en su parte superior derecha algunos números que comenzaron a cambiar (una cuenta creciente).

Parecía extraño, pero siguió avanzando. Vio otra galleta, repitió la operación y obtuvo el mismo resultado: una clínica y la cuenta aumentó nuevamente.

Luego entendió que era una especie de desafío, como los que solía ver en los videojuegos. Eso lo conmovió y comenzó a buscar galletas en todos los recovecos para ganar puntos. La factura aumentó.

También notó que en el lado superior izquierdo del pasillo, había tres círculos verdes. En el camino, encontró algo que no había visto hasta ahora: una planta en una maceta enorme.

Parecía normal, pero estaba algo fuera de lugar. Se acercó, la tocó, la planta pareció cobrar vida y se arrojó sobre él. Solo pudo ver enormes dientes afilados y el siguiente segundo: oscuridad.

Se despertó justo al comienzo del pasillo donde estaba el piso. La volvió a ver pero esta vez no la tocó. Se dio cuenta de que solo quedaban dos círculos verdes en la esquina superior izquierda.

Avanzó entonces y volvió a ver varias ollas como las primeras, pero las ignoró y las esquivó a todas.

De repente encontró una puerta diferente a las anteriores. Lo abrió y la atmósfera cambió; las paredes ya no eran azules, sino que brillaban verdes y el piso ya no era sólido, sino que era una especie de red que formaba un puente colgante.

Era un puente muy estrecho donde solo podía caminar con un pie delante del otro.

Al intentar cruzarlo, comenzaron a emerger desde abajo una especie de dardos que amenazaban con derribarlo. Uno tuvo éxito.

Se despertó nuevamente frente a la puerta singular. Lo abrió y nuevamente el puente. Levantó la vista y solo quedaba un círculo verde en el lado izquierdo.

Respiró hondo y se preparó para cruzar. Se las arregló para llegar al otro extremo y había otra puerta.

Lo abrió y encontró algunas piezas de metal flotando como nubes suspendidas. Esas piezas de metal formaron un camino.

Para cruzar ese espacio, tuve que saltar de un paso a otro. Lo hizo, y a medio camino comenzó a notar que los dardos de diferentes direcciones ahora caían.

Pero Daniel se concentró, saltó y saltó hasta que llegó a la meta. Otra puerta. Cuando abrió esta puerta vio una luz muy brillante que no puedo resistir. Tuvo que cerrar los ojos.

Cuando los abrió de nuevo, estaba en el suelo mirando el techo de la tienda. Había mucha gente a su alrededor examinándolo.

Daniel había recibido una descarga eléctrica mientras navegaba por la máquina rota.

Todos creían que había sido una experiencia dolorosa, pero Daniel sintió que había sido la aventura de su vida. ¿Qué videojuego había jugado?