Very good days:
Given the difficult and hard times we have experienced in recent weeks, our Government is driving and is advancing on 5 fronts simultaneously.
First, after listening with attention and humility to the voice of the people and the legitimate demands for a country with greater justice, with greater equity, with less abuse and with less privileges, we are promoting a Social Agenda that includes
A 50% increase in Basic Solidarity Pensions and Solidarity Pension Contribution. This will favor 1 million 600 thousand Chileans.
In addition, a 50% reduction in the cost of public transportation for our seniors.
A significant reduction in the price of medicines and an improvement in the quality and timing of health care.
A reduction in the prices of electricity, of public transport, a freeze in the prices of tolls.
And, in addition, the creation of a Minimum Guaranteed Income that does not exist in Chile, of $ 350 thousand per month.
These are part of the components of the Social Agenda that we hope will soon be able to alleviate and improve the quality of life of millions and millions of compatriots.
Second, and with the valuable collaboration of a large group of parliamentarians who reached a historic agreement one morning on Friday, we are moving forward on a Road Map that will allow us to have a New Constitution, in which citizens will have An active and effective participation.
Third, we are pushing an agenda to lower the diet of parliamentarians and the highest incomes of the public sector and, at the same time, strongly combat abuses and privileges, and most especially the monopolistic or anti-competitive behaviors that harm our citizens. And, in addition, strengthen the capacity of Chilean society to defend the rights of workers and consumers with greater opportunity and efficiency.
Fourth, we are promoting an agenda to strengthen public order, citizen security and social peace, and for that it is very important and I ask it with respect to Congress to accelerate the discussion and approval of laws that we have presented as Government for months and In some cases more than a year and they are very important to recover public order and citizen security, such as:
 
The Police Modernization Law
The Law that creates a new National Intelligence System
The Law that creates a Statute for the Protection of Our Police
The Anti-Hooded Law
The Anti-Stacking and Vandalism Law
The Anti-Barricade Law
All these laws will give us more and better instruments to protect you, your families, so that you can live in peace, you can study in peace, you can work in peace and you can live in peace.
Today we are going to take another step because we will sign and send to Congress and with the utmost urgency a bill that will allow our Armed Forces to collaborate in the protection of the critical infrastructure of our country. That critical infrastructure that is essential for the lives of millions and millions of compatriots and is essential for the normal development of our country.
This law will have two great benefits: first, to better protect that critical infrastructure for Chileans, such as transmission lines and power plants so that electricity is not cut off; such as, for example, drinking water plants so that we all have this supply; telecommunications systems; the hospitals; the ports; The airports.
I want to say that in these last 35 days, much of our basic infrastructure related to electricity supply, drinking water, telecommunications and health has suffered violent attacks that have put these basic services at risk.
But there is a second benefit of this law, which is that it will allow many carabineros to be freed so that instead of protecting their strategic places they can be on the streets, in the squares, in the parks, protecting the safety of citizens, from the families and homes of Chileans.
In this way we will be able to concentrate police officers in the work of public order and to have a greater police presence.
And I want to make it very clear, this law that allows the collaboration of the Armed Forces will not mean any restriction to the freedoms and rights of Chileans as provided by the States of Constitutional Exception.
But, in addition, it will not mean that the Armed Forces are involved or participate in public order, that task will be based on our Order and Security Forces.
And finally, the fifth point or pillar of our action as a Government. In the next few days we will present to the Congress and all Chileans a reconstruction plan of everything that the violentists have destroyed in recent times and that are such important works for Chileans. And, additionally, a plan to boost our economy that, without a doubt, has been severely affected by the violent events of recent weeks and public disorders of recent weeks.
What we will do with this Economic Re-Boost Plan is to recover our ability to grow, create jobs, improve wages, create opportunities for SMEs and, in this way, we will be able to get our economy to stand up. and recover your course towards progress and development.
We sincerely believe that with these five pillars that the Government is promoting, we are acting and listening with humility and attention to the voice of the people and we are making our best efforts to contribute with attitudes and concrete actions to those three great agreements that we proposed to Chileans: the Agreement for Peace, the Agreement for Social Justice and the Agreement for a New Constitution can advance with the participation and support of the vast majority of Chileans.
But for this to be a reality it is absolutely essential that all of us who believe in democracy, all of us who love peace, join forces and our wills to recover social peace and to categorically condemn all types of violence, wherever it comes from. .
Thank you very much.